﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineEncoding
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        string lineEncoding(string s) {
            var dic = new Dictionary<char, int>();
            var sb = new StringBuilder();
            
            for(int i = 0; i < s.Length; i++){
                if(dic.ContainsKey(s[i])){
                    dic[s[i]]++;
                }else{
                    if(i > 0){
                        if(dic[s[i - 1]] > 1){
                            sb.Append(dic[s[i - 1]]);
                        }
                        sb.Append(s[i - 1]);
                        
                        dic.Remove(s[i - 1]);
                    }
                    dic[s[i]] = 1;
                }
            }
            if(dic[s[s.Length - 1]] > 1){
                sb.Append(dic[s[s.Length - 1]]);
            }
            sb.Append(s[s.Length - 1]);
            
            return sb.ToString();
        }

    }
}
